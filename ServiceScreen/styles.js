import React, {PureComponent} from 'react';
import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  logoContainer1: {
    display: 'flex',
    flex: 1,
    justifyContent: 'center',
    flexDirection: 'column',

    alignItems: 'center',
    paddingTop: 10,
    paddingBottom: 10,
  },
  logoContainer: {
    flexDirection: 'column',
    backgroundColor: 'white',
    alignItems: 'center',
  
    height: '100%'
  },

  btnProp: {
    
      borderRadius: 10,
      paddingVertical: 15,
      paddingHorizontal: 15,
      borderRadius: 25,
      borderWidth: 2,
      width: 272,
      height: 40,
    borderColor:"#253780"
  },
  btnTextProp: {
    //color: 'gold',
    textAlign: 'center',
    fontSize: 20,
    alignItems: 'center',
    marginBottom: 15,
    color:"#253780"
  },
  viewPadding: {
    paddingBottom: 10,
    paddingTop: 10,
  },
  chatBtn: {
    borderRadius: 25,
    width: 95,
    height: 51,
    backgroundColor: 'black',
    flexDirection: 'column-reverse',
    marginRight: 100,
  },
  btn: {
    backgroundColor: 'rgba(52, 52, 52, 0.0)',
  },
});
export default styles;
