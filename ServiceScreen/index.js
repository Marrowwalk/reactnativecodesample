import React from 'react';
import {View, Image, Text, TouchableOpacity} from 'react-native';

import {useNavigation} from '@react-navigation/native';

import CustomButton from '../CustomButton';
import styles from './styles';
let splashImg = require('../../../images/logo.png');

//user component with options of choosing services (driver), account edit
const ServiceScreen = () => {
  const navigation = useNavigation();

  return (
    <View style={styles.logoContainer}>
      <View style={{padding: 40}}>
        <Image source={splashImg}></Image>
      </View>

      <View style={styles.viewPadding}>
        <TouchableOpacity
          style={{...styles.btnProp}}
          onPress={() => navigation.navigate('SearchConfirm')}>
          <View style={styles.logoContainer1}>
            <Text style={{...styles.btnTextProp}}>Driver</Text>
          </View>
        </TouchableOpacity>
      </View>
      <View style={styles.viewPadding}>
        <CustomButton title="Food" color="#EB9657" borderColor="#EB9657" />
      </View>
      <View style={styles.viewPadding}>
        <CustomButton title="Parcels" color="#992C17" borderColor="#992C17" />
      </View>
      <View style={styles.viewPadding}>
        <CustomButton title="Shopping" color="#F5CC00" borderColor="#F5CC00" />
      </View>
      <View style={styles.viewPadding}>
        <CustomButton title="Service" color="#696865" borderColor="#696865" />
      </View>
      <View style={styles.viewPadding}>
        <TouchableOpacity
          style={{...styles.btnProp, borderColor: '#47050D'}}
          onPress={() => navigation.navigate('MyAccountScreen')}>
          <View style={styles.logoContainer1}>
            <Text style={{...styles.btnTextProp, color: '#47050D'}}>
              My account
            </Text>
          </View>
        </TouchableOpacity>
      </View>

      <View>
        <TouchableOpacity>
          <View style={styles.logoContainer}>
            <Text>Chat</Text>
          </View>
        </TouchableOpacity>
        <View style={{padding: 10}}>
          <Text>Choose a service</Text>
        </View>
      </View>
    </View>
  );
};

export default ServiceScreen;
