import {StyleSheet} from 'react-native'

const styles = StyleSheet.create({
    pickerStyle: {
        backgroundColor: 'white',
        borderWidth: 1,
       // borderColor: 'white',
        borderRadius: 20,
        justifyContent: 'space-between',
        //color: 'black',
        width:'70%',
        alignSelf:  'center'
      },
      pickerSize: {
        height: 30,
        width: 150,
        justifyContent: 'space-between',
      },
      textInput: {
        height: 49,
    
        backgroundColor: '#eee',
        marginVertical: 10,
        borderColor: '#253780',
        borderWidth: 2,
        borderRadius: 10
      },
      btn: {
        width: 154,
        height: 44,
        borderRadius: 10,
        backgroundColor: '#253780',
        
      },
      btnLater: {
        width: 154,
        height: 44,
        borderRadius: 10,
        backgroundColor: 'white',
        borderColor: '#253780'
        
      },
      button: {
        width: 250,
        height: 35,
        marginBottom: 30,
        borderRadius: 5,
        justifyContent: 'center',
        alignContent: 'center',
      },
      buttonContainer: {
        marginBottom: 15,
        marginLeft: 80,
      },
})

export default styles