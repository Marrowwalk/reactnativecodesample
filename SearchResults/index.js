import React, {useState} from 'react';
import {View, Dimensions} from 'react-native';
import RouteMap from '../../components/RouteMap';
import {useRoute} from '@react-navigation/native';
import SearchResultsDistanceComponent from '../SearchResultsDistanceComponent';

const SearchResults = () => {
  //waiting for SocketIO
  const [] = useState('...');
  const [] = useState('...');
  const [] = useState('');
  const [] = useState('Premium');
  const [] = useState(false);

  const [] = useState(false);
  const GOOGLE_DISTANCES_API_KEY = '****';

  const route = useRoute();


  //logging the data for time/distance calculaton
  console.log(route.params);
  const {originPlace, destinationPlace} = route.params;
  console.log('Origin  place from search results: ' + Object.keys(originPlace));
  console.log(
    'Origin  place data from search results: ' +
      originPlace.data.geometry.location.lat,
  );
  console.log(
    'Origin  place data from search results: ' +
      originPlace.data.geometry.location.lng,
  );
  console.log(
    'Destination  place data from search results: ' +
      destinationPlace.details.geometry.location.lat,
  );
  console.log(
    'Destination  place data from search results: ' +
      destinationPlace.details.geometry.location.lng,
  );

  return (
    <View style={{display: 'flex', justifyContent: 'space-between'}}>
      <View style={{height: Dimensions.get('window').height - 350}}>
        <RouteMap origin={originPlace} destination={destinationPlace} />
      </View>
      <View style={{height: 400, backgroundColor: 'white', marginTop: 50}}>
        <SearchResultsDistanceComponent />
      </View>
    </View>
  );
};

export default SearchResults;
