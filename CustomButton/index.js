import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import styles from './styles'

//custom button used in profile and service screen
export default function CustomButton({
  title,
  color,
  borderColor,
}) {
  return (
    <View>
      <TouchableOpacity style={[styles.btnProp, borderColor && {borderColor}]} >
        <View style={styles.logoContainer}>
          <Text style={[styles.btnTextProp, color && {color}]}>{title}</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
}

