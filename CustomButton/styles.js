import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  logoContainer: {
    display: 'flex',
    flex: 1,
    justifyContent: 'center',
    flexDirection: 'column',

    alignItems: 'center',
    paddingTop: 10,
    paddingBottom: 10,
  },
  btnProp: {
    borderRadius: 10,
    paddingVertical: 15,
    paddingHorizontal: 15,
    borderColor: 'gold',
    borderRadius: 25,
    borderWidth: 2,
    width: 272,
    height: 40,
  },
  btnTextProp: {
    color: 'gold',
    textAlign: 'center',
    fontSize: 20,
    alignItems: 'center',
    marginBottom: 15,
  },
  viewPadding: {
    paddingBottom: 10,
    paddingTop: 10,
  },
});

export default styles;
