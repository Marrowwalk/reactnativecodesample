import React, {useState} from 'react';
import {View, Image, Button, Text, TextInput} from 'react-native';
import axios from 'axios';
import {useNavigation} from '@react-navigation/native';
import styles from './styles';
import AsyncStorage from '@react-native-async-storage/async-storage';

let splashImg = require('../../../../src/images/logo.png');

//user login component. Fetch token, save token to async storage
const LoginPage = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const navigation = useNavigation();

  //navigation test
  const navigationFunc = () => {
    navigation.navigate('ServiceScreen');
  };

  //login test
  const handleLogin = async () => {
    try {
      const response = await axios({
        method: 'post',
        url: '*******',
        headers: {},
        data: {
          email: email,
          password: password,
        },
      });
      alert('You are logged in!');

      //fetch resut test
      console.log('Response ' + response);

      try {
        const jsonValue = response.data.accessToken;
        AsyncStorage.setItem('token', jsonValue);
        console.log('Token from login: ' + jsonValue);
       
      } catch (e) {
        console.log(e);
      }
      this.getMyObject;
      navigationFunc();
    } catch (error) {
      alert(error);
      console.log(error);
    }
  };

  return (
    <View style={{backgroundColor: 'white'}}>
      <View>
        <Image source={splashImg} style={styles.image}></Image>
      </View>

      <View style={styles.internalContainer}>
        <View style={{padding: 10, marginTop: 170}}>
          <View>
            <Text style={{...styles.labelStyle, alignSelf: 'center'}}>
              ENTRANCE
            </Text>
          </View>
          <View>
            <Text style={styles.labelStyle}>Email</Text>
          </View>

          <View>
            <TextInput
              placeholder="Enter email address"
              style={styles.textInputStyle}
              autoCapitalize="none"
              onChangeText={email => setEmail(email)}
              defaultValue={email}
            />
          </View>
          <View>
            <Text style={styles.labelStyle}>Password</Text>
          </View>
          <View>
            <TextInput
              placeholder="Enter Password"
              style={styles.textInputStyle}
              onChangeText={password => setPassword(password)}
              defaultValue={password}
            />
          </View>
          <View style={styles.button}>
            <Button
              title="---->"
              color="#8B6368"
              onPress={() => handleLogin}
            />
          </View>
          <View style={{alignSelf: 'center'}}>
            <Text style={styles.labelStyle}>Or</Text>
          </View>
        </View>
        <View style={{marginBottom: 100, margin: 10}}>
          <View style={styles.button}></View>
          <View style={{flexDirection: 'row'}}>
            <View style={{width: 150, marginLeft: 30}}>
              <Button
                title="Google"
                color="orange"
                //onPress={() => navigation.navigate("ServiceScreen")}
              />
            </View>
            <View style={{width: 150, paddingBottom: 10}}>
              <Button
                title="Facebook"
                color="blue"
                //onPress={() => navigation.navigate("ServiceScreen")}
              />
            </View>
          </View>
          <View style={{marginBottom: 100, paddingBottom: 15}}>
            <View style={styles.button}>
              <Button
                title="No Account? Create one"
                color="black"
                onPress={() => navigation.navigate('RegisterPage')}
              />
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};

export default LoginPage;
