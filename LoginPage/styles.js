import React from 'react';
import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: 'white',
    flexDirection: 'column',
    alignSelf: 'center',
    alignContent:'center',

  },
  internalContainer: {
    width: '100%',
    height: 500,
    backgroundColor: 'black',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    alignContent: 'center',
    justifyContent: 'center',
    marginTop: 80,
    
  },
  image: {
    height: 112,
    width: 242,
    padding: 10,
    marginTop: 80,
    alignSelf:'center'
  },
  textInput: {
    height: 50,
    backgroundColor: '#eee',
    marginVertical: 10,
    borderColor: 'black',
    borderWidth: 5,
  },
  textStyle: {
    color: 'white',
    alignSelf: 'center',
    padding: 10,
  },

  button: {
    width: 250,
    height: 35,

    borderRadius: 5,
    marginLeft: 60,
    marginTop: 20,
  },
  buttonContainer: {
    marginBottom: 0,
    marginLeft: 80,
  },
  textInputStyle: {
    height: 35,
    borderColor: 'white',
    borderWidth: 1,
    borderRadius: 10,
    backgroundColor: 'white',
  },
  labelStyle: {
    color: 'white',
    marginTop: 10,
  },
});

export default styles;
