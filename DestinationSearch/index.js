import React, {useState, useEffect} from 'react';
import {View, SafeAreaView, TouchableOpacity, Text, Image} from 'react-native';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import { useNavigation } from '@react-navigation/native';

import styles from './styles.js';
import PlaceRow from "./PlaceRow";

const homePlace = {
  description: 'Home',
  geometry: { location: { lat: 50.234388, lng: 30.731025 } },
};
const workPlace = {
  description: 'Work',
  geometry: { location: { lat: 48.8496818, lng: 2.2940881 } },
};
const bottomImg = require('../../images/logo3.png');

const DestinationSearch = () => {
  const [originPlace, setOriginPlace] = useState(null);
  const [destinationPlace, setDestinationPlace] = useState(null);

  const navigation = useNavigation();

  const checkNavigation = () => {
    if (originPlace && destinationPlace) {
      navigation.navigate('SearchResults', {
        originPlace,
        destinationPlace,
      })
    }
  }

  useEffect(() => {
    checkNavigation();
  }, [originPlace, destinationPlace]);

  return (
    <SafeAreaView>
      <View style={styles.container}>
        <View style={{height: 66, width: 170, borderRadius: 10, backgroundColor:'#253780', alignSelf:'center'}}>
          <Text style={{color:'white', fontSize:20, textAlign:'center',textAlignVertical: "center"}}>USER</Text>
        </View>

        <GooglePlacesAutocomplete
          placeholder="Position"
          onPress={(data, details = null) => {
            setOriginPlace({data, details});
           
            //data from google places
            console.log('Data '+ Object.keys(data))
            console.log('Details '+ Object.keys(details))
            console.log('Details location '+ details.geometry.location)
    
          }}
          enableHighAccuracyLocation={true}
          enablePoweredByContainer={false}
          suppressDefaultStyles
          currentLocation={true}
          currentLocationLabel='Current location'
          styles={{
            textInput: styles.textInput,
            container: styles.autocompleteContainer,
            listView: styles.listView,
            separator: styles.separator,
          }}
          keyboardShouldPersistTaps='handled'
          isRowScrollable={true}
          fetchDetails={true}
          fetchDetails
          query={{
            key: '******',
            language: 'en',
          }}
          renderRow={(data) => <PlaceRow data={data} />}
          renderDescription={(data) => data.description || data.vicinity}
          predefinedPlaces={[homePlace, workPlace]}
        />

        <GooglePlacesAutocomplete
          placeholder="Destination"
          onPress={(data, details = null) => {
            setDestinationPlace({data, details});
           
            console.log('Data destination '+ Object.keys(data))
            console.log('Details destination '+ Object.keys(details))
            console.log('Details location destination'+ details.geometry.location)
          }}
          enablePoweredByContainer={false}
          suppressDefaultStyles
          styles={{
            textInput: styles.textInput,
            container: {
              ...styles.autocompleteContainer,
              top: 120,
            },
            separator: styles.separator,
          }}
          fetchDetails
          query={{
            key: '****',
            language: 'en',
          }}
          renderRow={(data) => <PlaceRow data={data} />}
        />

<View
        style={{
          flexDirection: 'row',
          padding: 30,
          position: 'absolute',
          marginTop: 200,
        }}>
        <View style={styles.btn}>
          <TouchableOpacity>
            <Text
              style={{
                color: 'white',
                alignItems: 'center',
                alignSelf: 'center',
              }}>
              Add destination
            </Text>
          </TouchableOpacity>
        </View>
        <View style={{...styles.btn, marginLeft: 30}}>
          <TouchableOpacity onPress={() => navigation.navigate('SearchResults')}>
            <Text
              style={{
                color: 'white',
                alignContent: 'center',
                alignSelf: 'center',
              }}>
              Confirm
            </Text>
          </TouchableOpacity>
        </View>
      </View>
      <View style={{position: 'absolute',
          marginTop: 350, padding: 10}}>
        <Text>
          Recent destinations
        </Text>
      </View>
      <View style={{position: 'absolute',
          marginTop: 450, padding: 20, alignSelf:'center'}}>
         <Image source={bottomImg} style={{height: 113, width: 153, marginBottom:0}}></Image>
      </View>
      </View>
    </SafeAreaView>
  );
};

export default DestinationSearch;
