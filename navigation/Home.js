import React from "react";
import { createStackNavigator } from '@react-navigation/stack'
import DestinationSearch from "../screens/DestinationSearch";
import SearchResults from "../screens/SearchResults";
import ServiceScreen from '../components/EntranceScreen/ServiceScreen'

const Stack = createStackNavigator();

const HomeNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      <Stack.Screen name={"DestinationSearch"} component={DestinationSearch} />
      <Stack.Screen name={"SearchResults"} component={SearchResults} />
      <Stack.Screen name={"ServiceScreen"} component={ServiceScreen} />
    </Stack.Navigator>
  );
};

export default HomeNavigator;
